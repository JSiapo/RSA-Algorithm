'''

Algoritmo de encriptación RSA
Autor:ProgramacionJS

'''

import RSA

def get_message():
    f = open('input.txt', 'r')
    message = f.read()
    f.close()
    return message

def write_message(message):
    f=open('output.txt','w')
    f.write(str(message[0]))
    for i in range (1,len(message)):
        f.write("\n"+str(message[i]))
    f.close()

print("Algorithm RSA")

keys = RSA.generate_key_public_private()  # Genero claves publica y privada

print("\nAlice sends a message to Bob")

print("Public Key: ({},{})".format(keys[0],keys[1]))
print("Private Key: {}".format(keys[2]))

message_encrypt = RSA.encrypt(get_message(), keys[0], keys[1])  # Mensaje encriptado

print("\nMessage with encrypt:\n--------------------------")
print(message_encrypt)

print("\nBob receives a message from Alice")
print("\nMessage with encrypt:\n--------------------------")
print(message_encrypt)
message_desencrypt=RSA.desencrypt(message_encrypt,keys[2],keys[0])
print("\nMessage desencrypt:\n--------------------------")
print(message_desencrypt)

write_message(message_encrypt)