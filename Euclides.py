'''

Algoritmo de Euclidiano
Autor:ProgramacionJS

'''

def gcd(a,b):
    while b!=0:
        r=divmod(a,b)[1]
        a=b
        b=r
    return a

def gcd_extends(a,b):
    d=0
    x=0
    y=0
    if b==0:
        d=a
        x=1
        y=0
        return (d, x, y)
    x1=0
    x2=1
    y1=1
    y2=0
    while b>0:
        q=divmod(a,b)[0]
        r=a-(q*b)
        x=x2-(q*x1)
        y=y2-(q*y1)
        a=b
        b=r
        x2=x1
        x1=x
        y2=y1
        y1=y
        d=a
        x=x2
        y=y2
    return (d,x,y)