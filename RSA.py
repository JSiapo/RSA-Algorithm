'''

Algoritmo de encriptación RSA
Autor:ProgramacionJS

'''

from random import randint

import MillerRabin
import Euclides

def generate_key_public_private():
    p = randint(100000, 1000000)
    q = randint(100000, 1000000)

    if MillerRabin.miller_rabin(p) == False or MillerRabin.miller_rabin(q) == False:
        while MillerRabin.miller_rabin(p) == False or MillerRabin.miller_rabin(q) == False:
            p = randint(100000, 1000000)
            q = randint(100000, 1000000)

    n = p * q
    phi = (p - 1) * (q - 1)

    e = randint(1, phi)
    if Euclides.gcd(e, phi) != 1:
        while Euclides.gcd(e, phi) != 1:
            e = randint(1, phi)
    d = Euclides.gcd_extends(e, phi)[1]
    if d > phi or d < 0:
        while d > phi or d < 0:
            e = randint(1, phi)
            if Euclides.gcd(e, phi) != 1:
                while Euclides.gcd(e, phi) != 1:
                    e = randint(1, phi)
            d = Euclides.gcd_extends(e, phi)[0]

    print("\np: {}".format(p))
    print("q: {}".format(q))

    print("\nphi: {}".format(phi))
    return n, e, d


def encrypt(message,n,e):
    message_ascii=[]
    print("\nMessage without encrypt:\n-------------------------")
    print(message)
    for character in message:
        message_ascii.append(ord(character))
    message_encrypt=[]
    for i in range (0,len(message_ascii)):
        message_encrypt.append(pow(message_ascii[i],e,n))
    return message_encrypt

def desencrypt(message_encrypt,d,n):
    message_aux = ""
    message = ""
    for i in range (0,len(message_encrypt)):
        message_aux=chr(pow(message_encrypt[i],d,n))
        message=message+message_aux
    return message